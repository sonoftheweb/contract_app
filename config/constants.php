<?php

return [
    'roles' => [
        'account_manager' => 1,
        'account_client' => 2
    ],

    'plans' => [
        'free_plan' => [
            'name' => 'Starter Plan',
            'plan_id' => 'free_plan',
            'min_seats' => 1,
            'max_seats' => 1,
            'tenure' => 'month',
            'tiers' => [
                [
                    'price' => 0
                ]
            ],
            'features' => [
                'contract_count' => 5,
                'client_count' => 5,
                'live_contract_collaboration' => false,
                'spell_checker' => false,
                'extended_editor' => false,
                'contract_template_manager' => false,
                'contract_buddy' => false
            ]
        ],
        'silver_monthly_plan' => [
            'name' => 'Monthly Silver Plan',
            'plan_id' => 'silver_monthly_plan',
            'min_seats' => 3,
            'max_seats' => 10,
            'tenure' => 'month',
            'tiers' => [
                [
                    'upto' => 3,
                    'price' => 6000
                ],
                [
                    'price' => 1800
                ]
            ],
            'features' => [
                'contract_count' => 50,
                'client_count' => 50,
                'live_contract_collaboration' => true,
                'spell_checker' => true,
                'extended_editor' => true,
                'contract_template_manager' => true,
                'contract_buddy' => false
            ]
        ],
        'silver_annual_plan' => [
		            'name' => 'Annual Silver Plan',
            'plan_id' => 'silver_annual_plan',
            'min_seats' => 3,
            'max_seats' => 10,
            'tenure' => 'year',
            'tiers' => [
                [
                    'upto' => 3,
                    'price' => 64800
                ],
                [
                    'price' => 19450
                ]
            ],
            'features' => [
                'contract_count' => 50,
                'client_count' => 50,
                'live_contract_collaboration' => true,
                'spell_checker' => true,
                'extended_editor' => true,
                'contract_template_manager' => true,
                'contract_buddy' => false
            ]
        ],
        'gold_monthly_plan' => [
            'name' => 'Monthly Gold Plan',
            'plan_id' => 'gold_monthly_plan',
            'min_seats' => 11,
            'max_seats' => 30,
            'tenure' => 'month',
            'tiers' => [
                [
                    'upto' => 11,
                    'price' => 10000
                ],
                [
                    'price' => 9000
                ]
            ],
            'features' => [
                'contract_count' => 100,
                'client_count' => 100,
                'live_contract_collaboration' => true,
                'spell_checker' => true,
                'extended_editor' => true,
                'contract_template_manager' => true,
                'contract_buddy' => true
            ]
        ],
        'gold_annual_plan' => [
            'name' => 'Annual Gold Plan',
            'plan_id' => 'gold_annual_plan',
            'min_seats' => 11,
            'max_seats' => 30,
            'tenure' => 'year',
            'tiers' => [
                [
                    'upto' => 11,
                    'price' => 10800
                ],
                [
                    'price' => 97200
                ]
            ],
            'features' => [
                'contract_count' => 100,
                'client_count' => 100,
                'live_contract_collaboration' => true,
                'spell_checker' => true,
                'extended_editor' => true,
                'contract_template_manager' => true,
                'contract_buddy' => true
            ]
        ]
    ]
];
