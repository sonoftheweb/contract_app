<?php

namespace App\Console\Commands;

use Artisan;
use Illuminate\Console\Command;

class setup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs full setup of application for development.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->warn('Setting up telescope.');

        Artisan::call('telescope:install');

        $this->warn('Running fresh migrations and seeders.');

        Artisan::call('migrate:fresh --seed');

        $this->warn('Setting up passport.');

        Artisan::call('passport:install');

        $this->warn('IDE helper refreshing');

        Artisan::call('ide-helper:generate');

        $this->info('All done!');
    }
}
