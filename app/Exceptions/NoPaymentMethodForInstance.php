<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Contracts\Support\Responsable;

class NoPaymentMethodForInstance extends Exception implements Responsable
{

    public function toResponse($request)
    {
        return response([
            'status' => 'error',
            'error' => 'Your account does not have a registered payment method. Please add one before proceeding.'
        ], 401);
    }
}
