<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Contracts\Support\Responsable;
use Redirect;

class AuthenticationFailed extends Exception
{

    public function render($request)
    {
        $error = [
            'error' => 'Unable to authenticate the user'
        ];

        return Redirect::route('login')->with('error', $error);;
    }
}
