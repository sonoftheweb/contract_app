<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Contracts\Support\Responsable;

class InstanceNotSubscribedException extends Exception implements Responsable
{

    public function toResponse($request)
    {
        return redirect()->route('subscription.index');
    }
}
