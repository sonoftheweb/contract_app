<?php

namespace App\Exceptions;

use Auth;
use Exception;
use Illuminate\Contracts\Support\Responsable;

class NoAuthRoles extends Exception implements Responsable
{

    public function toResponse($request)
    {
        Auth::logout();

        return redirect()->route('/')->with('errors', 'User is not attached to an account. Contact admin to rectify this issue.');
    }
}
