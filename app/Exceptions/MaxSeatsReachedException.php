<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Contracts\Support\Responsable;

class MaxSeatsReachedException extends Exception implements Responsable
{
 
	public function toResponse($request)
	{
		return redirect()->route('users.index')->with('message', 'You do not have enough seats to add this user. Kindly make room by removing users or upgrade your account.');
	}
}
