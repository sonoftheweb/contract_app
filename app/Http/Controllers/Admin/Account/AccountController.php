<?php

namespace App\Http\Controllers\Admin\Account;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\ControllerDefaults;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class AccountController extends Controller
{
	use ControllerDefaults;
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
	    $data = [
		    'title' => 'Account details',
	    ];
	
	    $this->buildDefaults($data);
	
	    return Inertia::render('Admin/Account/Account', $this->defaults);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
