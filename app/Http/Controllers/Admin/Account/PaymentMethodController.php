<?php

namespace App\Http\Controllers\Admin\Account;

use App\Exceptions\UserHasNoInstance;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\ControllerDefaults;
use App\Http\UseCase\HandlesPayment;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;
use Laravel\Cashier\Cashier;
use Laravel\Cashier\Payment;
use Laravel\Cashier\PaymentMethod;
use Stripe\BankAccount;
use Stripe\Card;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent as StripePaymentIntent;

class PaymentMethodController extends Controller
{
    use ControllerDefaults;

    /**
     * Display a listing of the resource.
     *
     * @param HandlesPayment $handlesPayment
     * @return Response
     */
    public function index(HandlesPayment $handlesPayment)
    {
        $intent = $handlesPayment->getSetupIntent();

        $data = [
            'title' => 'Update payment method',
            'intent' => $intent,
            'for_payment' => false
        ];

        $this->buildDefaults($data);

        return Inertia::render('Admin/Account/AddPaymentMethod', $this->defaults);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param HandlesPayment $handlesPayment
     * @return RedirectResponse|Response
     * @throws UserHasNoInstance
     */
    public function store(HandlesPayment $handlesPayment)
    {
        $handlesPayment->savePaymentMethod();

        if (!request()->post('redirect')) {
            $paymentMethod = $handlesPayment->getDefaultPaymentMethod();
            $customerId = $handlesPayment->getCustomerId();

            $data = [
                'title' => 'Processing payment...',
                'payment_data' => request()->post('payment_data'),
                'payment_method' => $paymentMethod,
                'customer_id' => $customerId
            ];

            $this->buildDefaults($data);

            return Inertia::render('Admin/Account/ProcessingPayment', $this->defaults);
        }

        return redirect()->route('subscription.index')->with('response', [
            'message' => 'Successfully saved payment method'
        ]);
    }
}
