<?php

namespace App\Http\Controllers\Admin\Account;

use App\Exceptions\UserHasNoInstance;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\ControllerDefaults;
use App\Http\UseCase\HandlesPayment;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;
use Laravel\Cashier\Cashier;
use Laravel\Cashier\Exceptions\IncompletePayment;
use Laravel\Cashier\Exceptions\PaymentActionRequired;
use Laravel\Cashier\Exceptions\PaymentFailure;
use Laravel\Cashier\Exceptions\SubscriptionUpdateFailure;
use Laravel\Cashier\Payment;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;

class PaymentController extends Controller
{
    use ControllerDefaults;

    /**
     * Display a listing of the resource.
     *
     * @param HandlesPayment $handlesPayment
     * @return Response
     * @throws UserHasNoInstance|ApiErrorException
     */
    public function beginPayment(HandlesPayment $handlesPayment)
    {
        $intent = $handlesPayment->getPaymentIntent([
	        'amount' => request()->post('amount'),
	        'currency' => 'usd',
	        'setup_future_usage' => 'off_session',
	        'statement_descriptor' => 'FortCon Payment.'
        ]);

        $data = [
            'title' => 'Make payment',
            'intent' => $intent,
            'for_payment' => true,
            'payment_data' => [
                'amount' => request()->post('amount'),
                'plan_id' => request()->post('plan'),
                'seats' => request()->post('seats'),
                'default_payment_method' => $handlesPayment->getDefaultPaymentMethod()
            ]
        ];

        $this->buildDefaults($data);

        return Inertia::render('Admin/Account/AddPaymentMethod', $this->defaults);
    }

    /**
     * @param HandlesPayment $handlesPayment
     * @return RedirectResponse|Response
     * @throws PaymentFailure
     * @throws UserHasNoInstance|ApiErrorException|IncompletePayment
     */
    public function makePayment(HandlesPayment $handlesPayment)
    {
        try {
            $handlesPayment->makePayment();
        } catch (PaymentActionRequired $e) {
	        $payment = new Payment(
		        PaymentIntent::retrieve($e->payment->id, Cashier::stripeOptions())
	        );
	
	        $data = [
		        'title' => 'Payment Confirmation',
		        'payment' => $payment->asStripePaymentIntent()->toArray(),
		        'redirect' => route('subscription.index')
	        ];
	
	        $this->buildDefaults($data);
	
	        return Inertia::render('Admin/Account/ConfirmPayment', $this->defaults);
        }

        return redirect()->route('subscription.index');
    }
	
	/**
	 * @param $id
	 * @return Response
	 * @throws ApiErrorException
	 */
    public function confirm($id) {
        $payment = new Payment(
            PaymentIntent::retrieve($id, Cashier::stripeOptions())
        );

        $data = [
            'title' => 'Payment Confirmation',
            'payment' => $payment->asStripePaymentIntent()->toArray(),
            'redirect' => route('subscription.index')
        ];

        $this->buildDefaults($data);

        return Inertia::render('Admin/Account/ConfirmPayment', $this->defaults);
    }
}
