<?php

namespace App\Http\Controllers\Admin\Account;

use App\Exceptions\UserHasNoInstance;
use App\Helpers\InstanceHelper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\ControllerDefaults;
use App\Http\UseCase\HandlesPayment;
use App\Http\UseCase\HandlesSubscriptions;
use Inertia\Inertia;
use Inertia\Response;

class SubscriptionController extends Controller
{
    use ControllerDefaults;

    /**
     * Display subscription page.
     *
     * @param HandlesSubscriptions $handlesSubscriptions
     * @param HandlesPayment $handlesPayment
     * @return Response
     * @throws UserHasNoInstance
     */
    public function index(HandlesSubscriptions $handlesSubscriptions, HandlesPayment $handlesPayment)
    {
        if (request()->has('change-subscription')) {
            return $this->renderChangeSubscriptionPage();
        }

        return $this->renderSubscriptionPage($handlesSubscriptions, $handlesPayment);
    }

    /**
     * Render the change subscription page
     *
     * @return Response
     */
    private function renderChangeSubscriptionPage()
    {
        $data = [
            'title' => 'Change plan'
        ];

        $data['plans'] = config('constants.plans');

        $this->buildDefaults($data);

        return Inertia::render('Admin/Account/ChangeSubscription', $this->defaults);
    }

    /**
     * Renders the subscription page
     *
     * @param HandlesSubscriptions $handlesSubscriptions
     * @param HandlesPayment $handlesPayment
     * @return Response
     * @throws UserHasNoInstance
     */
    private function renderSubscriptionPage(HandlesSubscriptions $handlesSubscriptions, HandlesPayment $handlesPayment)
    {
        $data = [
            'title' => 'Manage Subscription',
            'subscription' => $handlesSubscriptions->getCurrentSubscription()->toArray(),
            'seats' => InstanceHelper::getInstance()->seats,
            'invoices' => $handlesSubscriptions->getInvoices(),
            'payment_method' => InstanceHelper::getInstance()->hasPaymentMethod() ? $handlesPayment->getPaymentMethod() : InstanceHelper::getInstance()->hasPaymentMethod(),
            'plans' => config('constants.plans'),
	        'subscribable_plans' => $this->availablePlans()
        ];

        $this->buildDefaults($data);

        return Inertia::render('Admin/Account/Subscription', $this->defaults);
    }

	private function availablePlans()
	{
		$plans = config('constants.plans');
		unset($plans['free_plan']);

		return  $plans;
	}
}
