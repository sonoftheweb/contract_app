<?php

namespace App\Http\Controllers\Admin\Account;

use App\Exceptions\MaxSeatsReachedException;
use App\Exceptions\UserHasNoInstance;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\ControllerDefaults;
use App\Http\Resources\UserCollection;
use App\Http\UseCase\HandlesUsers;
use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Inertia\Inertia;
use Inertia\Response;
use Redirect;

class UserController extends Controller
{
    use ControllerDefaults;

    protected $relationship_dependencies = [

    ];

    public function index(HandlesUsers $handlesUsers)
    {
        $data = [
        	'title' => 'Manage Users',
	        'users' => function () use ($handlesUsers) {
		        return new UserCollection($handlesUsers->getUsers());
	        }
        ];

        $this->buildDefaults($data);

        return Inertia::render('Admin/Account/Users', $this->defaults);
    }

	/**
	 * @param Request $request
	 * @return RedirectResponse
	 * @throws UserHasNoInstance|ValidationException|MaxSeatsReachedException
     */
	public function store(Request $request)
    {
    	$handlesUsers = new HandlesUsers($request);
    	$handlesUsers->addUser();

	    return Redirect::route('users.index')->with('message', 'User added!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param HandlesUsers $handlesUsers
     * @param int $id
     * @return RedirectResponse
     * @throws UserHasNoInstance
     */
	public function update(HandlesUsers $handlesUsers, int $id)
	{
		if (request()->has('status'))
			$handlesUsers->updateUserStatus($id);

		if (request()->has('user'))
			$handlesUsers->updateUserData($id);

		return Redirect::route('users.index')->with('message', 'User updated!');
	}

    /**
     * Deletes a user from the application.
     *
     * @param HandlesUsers $handlesUsers
     * @param $id
     * @return RedirectResponse
     * @throws UserHasNoInstance
     */
    public function destroy(HandlesUsers $handlesUsers, $id)
    {
        if (!Auth::user()->hasRole('account_manager')) {
            abort(404, 'No access rights to delete user.');
        }

        $handlesUsers->deleteUser($id);

        return Redirect::route('users.index')->with('message', 'User removed!');
    }
}
