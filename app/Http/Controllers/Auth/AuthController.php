<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\ControllerDefaults;
use App\Http\UseCase\HandlesAuth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Validation\ValidationException;
use Inertia\Inertia;
use Inertia\Response;

class AuthController extends Controller
{
    use ControllerDefaults;

    /**
     * Display a listing of the resource.
     *
     * @return Response | RedirectResponse
     */
    public function index()
    {
        return Inertia::render('Auth/Login', $this->defaults);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param HandlesAuth $handlesAuth
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function store(HandlesAuth $handlesAuth)
    {
        return ($handlesAuth->authenticateUser()) ? redirect()->route('dashboard.index') : redirect()->route('login.index');
    }
}
