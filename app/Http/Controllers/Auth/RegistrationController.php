<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\AuthenticationFailed;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\ControllerDefaults;
use App\Http\UseCase\HandlesRegistration;
use Illuminate\Http\RedirectResponse;
use Illuminate\Validation\ValidationException;
use Laravel\Cashier\Exceptions\CustomerAlreadyCreated;
use Laravel\Cashier\Exceptions\PaymentActionRequired;
use Laravel\Cashier\Exceptions\PaymentFailure;

class RegistrationController extends Controller
{
    use ControllerDefaults;

    /**
     * Store a newly created resource in storage.
     *
     * @param HandlesRegistration $handlesRegistration
     * @return RedirectResponse
     * @throws AuthenticationFailed
     * @throws ValidationException
     * @throws CustomerAlreadyCreated
     * @throws PaymentActionRequired
     * @throws PaymentFailure
     */
    public function store(HandlesRegistration $handlesRegistration)
    {
        $handlesRegistration->doRegister();

        return redirect()->route('login.index');
    }
}
