<?php


namespace App\Http\Controllers\Traits;


use Auth;

trait ControllerDefaults
{
    public $defaults = [
        'title' => 'Contract Application',
        'meta' => 'Lorem Ipsum'
    ];

    public function buildDefaults($data)
    {
        $this->defaults = array_merge($this->defaults, $data);
    }
}
