<?php


namespace App\Http\UseCase;


use App\Exceptions\MaxSeatsReachedException;
use App\Exceptions\UserHasNoInstance;
use App\Helpers\InstanceHelper;
use App\Models\Instance;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\UseCase\Traits\ModelRelationshipBinding;
use Illuminate\Validation\ValidationException;
use App\Helpers\SearchSortPaginateHelper;
use App\Models\User;
use Redirect;
use Str;

class HandlesUsers extends UseCase
{
	use ModelRelationshipBinding;

	protected $relationship_dependencies = [];

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * Get's a paginated list of users
	 *
	 * @param string|null $relationship
	 * @return mixed
	 * @throws UserHasNoInstance
	 */
	public function getUsers(string $relationship = null)
	{
		$query = User::query()
			->with('roles')
			->where('instance_id', InstanceHelper::getInstanceId());
		$query = $this->buildRelationshipToLoad($query, $relationship);
		return SearchSortPaginateHelper::searchSortAndPaginate($this->request, $query);
	}

	/**
	 * Updates status
	 *
	 * @param int $id
	 */
	public function updateUserStatus(int $id)
	{
		$user = User::query()->find($id);
		$user->status = $this->request->status;
		$user->save();
	}

	/**
	 * @param int $id
	 * @throws UserHasNoInstance
	 */
	public function updateUserData(int $id)
	{
		$user = User::query()
			->where('id', $id)
			->where('instance_id', InstanceHelper::getInstanceId())
			->with('userAttributes')
			->first();

		$user->name = $this->request->user['name'];
		$user->save();

		$user->userAttributes->contact_phone = $this->request->user['attributes']['contact_phone'];
		$user->userAttributes->save();
	}

	/**
	 * Add users to the db
	 * @throws ValidationException|UserHasNoInstance|MaxSeatsReachedException
     */
	public function addUser()
	{
		$this->validateUserData()->checkSeats()->persistUser();
	}

	/**
	 * @return $this
	 * @throws ValidationException
	 */
	private function validateUserData()
	{
		$this->validate($this->request, [
			'name' => 'required|min:5|max:30',
			'email' => 'required|unique:users,email|email:rfc,dns',
			'contact_phone' => 'required',
			'role' => 'required'
		]);

		return $this;
	}

	/**
	 * Saves the user to DB
	 *
	 * @throws UserHasNoInstance
	 */
	private function persistUser()
	{
		// generate password
		$password = bcrypt(Str::random(6));

		$this->user = User::query()->create([
			'name' => $this->request->name,
			'email' => $this->request->email,
			'password' => $password,
			'instance_id' => InstanceHelper::getInstanceId()
		]);

		$this->user->roles()->sync([
			config('constants.roles.'.$this->request->role) => [
				'instance_id' => InstanceHelper::getInstanceId()
			]
		]);

		$this->user->userAttributes()->create([]);

		$this->user->preference()->create([
			'preferences' => []
		]);

        if ($this->request->role === 'account_manager') {
            $this->instance->occupySeat();
        }

		return $this->user;
	}

	/**
	 * Checks if there are vacant seats for the user in this instance
	 * @throws UserHasNoInstance
	 * @throws MaxSeatsReachedException
	 */
	protected function checkSeats()
	{
		if ($this->request->role === 'account_manager') {
			$manager_user_count = User::query()
				->where('instance_id', InstanceHelper::getInstanceId())
				->whereHas('roles', function (Builder $builder) {
					$builder->where('roles.id', config('constants.roles.account_manager'));
				})->count();

			if ((int) $manager_user_count >= (int) $this->instance->seats) {
				throw new MaxSeatsReachedException;
			}
		}

		return $this;
	}

    /**
     * Deletes a user but not before checking if a user has concerns they are dealing with at the moment.
     *
     * @param int $id
     */
    public function deleteUser(int $id)
    {
        $user = User::query()->findOrFail($id);

        // check if a user has any contracts tied to them

        // check if a user is supposed to sign a contract

        // is the user a manager?
        if ($user->hasRole('account_manager')) {
            $this->instance->releaseSeat();
        }

        User::destroy($id);
    }
}
