<?php


namespace App\Http\UseCase;


use App\Exceptions\UserHasNoInstance;
use App\Helpers\InstanceHelper;
use App\Models\Instance;
use Laravel\Cashier\Cashier;
use Laravel\Cashier\Exceptions\IncompletePayment;
use Laravel\Cashier\Exceptions\PaymentActionRequired;
use Laravel\Cashier\Exceptions\PaymentFailure;
use Laravel\Cashier\Exceptions\SubscriptionUpdateFailure;
use Laravel\Cashier\Payment;
use Laravel\Cashier\PaymentMethod;
use Stripe\BankAccount;
use Stripe\Card;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent as StripePaymentIntent;
use Stripe\SetupIntent;

class HandlesPayment extends UseCase
{
    /**
     * Get's the first available payment method
     *
     * @return array
     * @throws UserHasNoInstance
     */
    public function getPaymentMethod()
    {
        $paymentMethod = [];

        $method = InstanceHelper::getInstance()->paymentMethods()->first();

        if ($method)
            $paymentMethod = [
                'id' => $method->id,
                'brand' => $method->card->brand,
                'last_four' => $method->card->last4,
                'exp_month' => $method->card->exp_month,
                'exp_year' => $method->card->exp_year,
            ];

        return $paymentMethod;
    }

    /**
     * @return array
     * @throws UserHasNoInstance
     */
    public function getDefaultPaymentMethod()
    {
        $method = InstanceHelper::getInstance()->defaultPaymentMethod();

        if (!$method)
            return null;

        return [
            'id' => $method->id,
            'brand' => $method->card->brand,
            'last_four' => $method->card->last4,
            'exp_month' => $method->card->exp_month,
            'exp_year' => $method->card->exp_year,
        ];
    }

    /**
     * Sets up payment intent
     *
     * @return SetupIntent
     */
    public function getSetupIntent()
    {
        return $this->instance->createSetupIntent();
    }

    /**
     * Saves payment method
     *
     * @return $this
     * @throws UserHasNoInstance
     */
    public function savePaymentMethod()
    {
        $currentDefault =  InstanceHelper::getInstance()->defaultPaymentMethod();

        if ($currentDefault && $currentDefault->id === $this->request->pm) {
            return $this;
        }

        $this->instance->updateDefaultPaymentMethod($this->request->pm);

        $this->instance->updateDefaultPaymentMethodFromStripe();

        return $this;
    }

    /**
     *
     * @throws PaymentFailure
     * @throws PaymentActionRequired|UserHasNoInstance|IncompletePayment
     */
    public function makePayment()
    {
        $paymentMethodId = $this->request->stripeIntent['payment_method'];
        $planId = $this->request->paymentData['plan_id'];
        $quantity = (int) $this->request->paymentData['seats'];
	
	    Instance::query()
		    ->find($this->instance->id)
		    ->update(['seats' => $quantity]);

        if ($this->instance->subscribed(InstanceHelper::subscriptionNameFromInstance($this->instance))) {
	        $this->instance->updateDefaultPaymentMethod($paymentMethodId);
	        $this->instance->updateDefaultPaymentMethodFromStripe();
	
	        // cancel subscription
	        $this->instance->subscription(InstanceHelper::subscriptionNameFromInstance($this->instance))
		        ->cancelNow();
        }
	
	    // create a new one with details provided
	    $this->instance->newSubscription(InstanceHelper::subscriptionNameFromInstance($this->instance), $planId)
		    ->withMetadata([
			    'account_name' => $this->instance->instance_name,
			    'account_email' => $this->instance->direct_email
		    ])
		    ->quantity($quantity)
		    ->create($paymentMethodId);

        return $this;
    }

    public function getCustomerId()
    {
        return $this->instance->stripeId();
    }

    /**
     * @param $params
     * @return array
     * @throws ApiErrorException
     */
    public function getPaymentIntent($params)
    {
        $PI = new Payment(StripePaymentIntent::create($params, Cashier::stripeOptions()));

        return $PI->asStripePaymentIntent()->toArray();
    }
}
