<?php


namespace App\Http\UseCase\Traits;


use Illuminate\Database\Eloquent\Builder;

trait ModelRelationshipBinding
{
	/**
	 * Builds a relationship into the query based on relationship (string) given. Relationship must be defined in model.
	 *
	 * @param Builder $query
	 * @param null $relationship
	 * @return Builder
	 */
	public function buildRelationshipToLoad(Builder $query, $relationship = null) : Builder
	{
		if (!$relationship)
			return $query;
		
		if (!array_key_exists($relationship, $this->relationship_dependencies))
			return $query;
		
		return $query->with($this->relationship_dependencies[$relationship]);
	}
}