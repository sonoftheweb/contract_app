<?php


namespace App\Http\UseCase;


use App\Exceptions\AuthenticationFailed;
use App\Exceptions\UserHasNoInstance;
use App\Helpers\InstanceHelper;
use App\Models\Instance;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\RedirectResponse;
use Illuminate\Validation\ValidationException;
use Laravel\Cashier\Exceptions\CustomerAlreadyCreated;
use Laravel\Cashier\Exceptions\PaymentActionRequired;
use Laravel\Cashier\Exceptions\PaymentFailure;

class HandlesRegistration extends UseCase
{
    /**
     * Perform all operation about user and account registration
     *
     * @throws AuthenticationFailed
     * @throws CustomerAlreadyCreated
     * @throws PaymentActionRequired
     * @throws PaymentFailure
     * @throws ValidationException|UserHasNoInstance
     */
    public function doRegister()
    {
        $this->validateRegistration()
            ->createInstance()
            ->subscribeToFree()
            ->createUser()
            ->authenticateUser();
    }

    /**
     * Validates the registration data sent
     *
     * @return $this
     * @throws ValidationException
     */
    public function validateRegistration()
    {
        $this->validate($this->request, [
            'user.name' => 'required|min:1|max:30',
            'user.email' => 'required|unique:users,email|email:rfc,dns',
            'user.password' => 'required|min:6|max:20',
            'user.confPassword' => 'required|min:6|max:20', // @todo Make this equal to password
            'instance.instance_name' => 'required|min:2|max:250',
            'instance.address_line_1' => 'required|min:5|max:250',
            'instance.address_line_2' => 'max:250',
            'instance.city' => 'required|min:5|max:250',
            'instance.state' => 'required|min:5|max:250',
            'instance.country' => 'required|min:5|max:250',
            'instance.website' => 'max:250',
            'instance.direct_phone' => 'max:17',
            'instance.direct_email' => 'required|email:rfc,dns'
        ]);

        return $this;
    }

    /**
     * Creates an instance
     *
     * @return $this
     */
    protected function createInstance()
    {
        $this->instance = Instance::query()->create($this->request->instance);

        return $this;
    }

    /**
     * Creates and instance / account
     *
     * @return $this
     * @throws UserHasNoInstance
     */
    protected function createUser()
    {
        $user_data = $this->request->user;

        $user_data['password'] = bcrypt($user_data['password']);

        unset($user_data['confPassword']);

        $this->user = $this->instance->users()->create($user_data);

        $this->user->roles()->sync([
        	config('constants.roles.account_manager') => [
        		'instance_id' => $this->instance->id
	        ]
        ]);

        $this->user->userAttributes()->create([]);

        $this->user->preference()->create([
            'preferences' => []
        ]);

        $this->instance->account_manager_user_id = $this->user->id;

        $this->instance->save();

        return $this;
    }

    /**
     * Authenticate the user
     *
     * @return RedirectResponse
     * @throws AuthenticationFailed
     */
    protected function authenticateUser()
    {
        $credentials = [
            'email' => $this->request->user['email'],
            'password' => $this->request->user['password'],
        ];

        if (!Auth::attempt($credentials))
            throw new AuthenticationFailed;

        return redirect()->intended('dashboard');
    }

    /**
     * @return $this
     * @throws CustomerAlreadyCreated
     * @throws PaymentActionRequired
     * @throws PaymentFailure|UserHasNoInstance
     */
    protected function subscribeToFree()
    {
        $this->instance->createAsStripeCustomer([
            'email' => $this->instance->direct_email,
            'name' => $this->instance->instance_name
        ]);

        $this->instance->newSubscription(InstanceHelper::subscriptionNameFromInstance($this->instance), 'free_plan')->withMetadata([
            'account_name' => $this->instance->instance_name,
            'account_email' => $this->instance->direct_email
        ])
            ->quantity(config('constants.plans.free_plan.min_seats'))
            ->create();

        return $this;
    }
}
