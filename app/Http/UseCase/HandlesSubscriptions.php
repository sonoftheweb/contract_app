<?php


namespace App\Http\UseCase;


use App\Exceptions\UserHasNoInstance;
use App\Helpers\InstanceHelper;
use Carbon\Carbon;
use Laravel\Cashier\Subscription;

class HandlesSubscriptions extends UseCase
{
    /**
     * Gets all subscriptions past and present
     *
     * @return mixed
     * @throws UserHasNoInstance
     */
    public function getAllSubscriptions()
    {
        return InstanceHelper::getInstance()->subscriptions()->get();
    }

    /**
     * Gets current subscription
     *
     * @return mixed
     * @throws UserHasNoInstance
     */
    public function getCurrentSubscription()
    {
        return $this->getAllSubscriptions()->first();
    }

    /**
     * Gets all invoices for current instance
     *
     * @throws UserHasNoInstance
     */
    public function getInvoices()
    {
        $stripeInvoices = InstanceHelper::getInstance()->invoicesIncludingPending();
        
        $invoices = [];
        foreach ($stripeInvoices as $invoice) {
            $invoices[] = [
                'number' => $invoice->number,
                'status' => $invoice->status,
                'starts' => Carbon::parse($invoice->period_start)->format('Y.m.d'),
                'ends' => Carbon::parse($invoice->period_end)->format('Y.m.d'),
                'tax' => $invoice->tax,
                'subtotal' => $invoice->subtotal(),
                'total' => $invoice->total(),
	            'payment_intent' => $invoice->payment_intent,
	            'subscription' => $invoice->subscription
            ];
        }

        return $invoices;
    }
}
