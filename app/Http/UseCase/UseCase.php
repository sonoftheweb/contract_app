<?php


namespace App\Http\UseCase;


use App\Exceptions\UserHasNoInstance;
use App\Helpers\InstanceHelper;
use App\Models\Instance;
use App\Models\User;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

class UseCase
{
    use ValidatesRequests;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Instance
     */
    protected $instance;

    /**
     * @var User
     */
    protected $user;

    /**
     * UseCase constructor.
     *
     * @param Request $request
     * @throws UserHasNoInstance
     */
    public function __construct(Request $request)
    {
        $this->request = $request;

        if (!Auth::guest()) {
            $this->instance = InstanceHelper::getInstance();
        }
    }

    /**
     * @return Instance
     */
    public function getInstance()
    {
        return $this->instance;
    }
}
