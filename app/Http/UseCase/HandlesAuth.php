<?php


namespace App\Http\UseCase;


use App\Exceptions\AuthenticationFailed;
use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Validation\ValidationException;

class HandlesAuth extends UseCase
{
    /**
     * Perform the whole process of authenticating the user
     *
     * @throws ValidationException
     */
    public function authenticateUser()
    {
        return $this->validatesUser()->logUserIn();
    }

    /**
     * Validates login data
     *
     * @return $this
     * @throws ValidationException
     */
    public function validatesUser()
    {
        $this->validate($this->request, [
            'email' => 'required|email|exists:users'
        ]);

        return $this;
    }

    /**
     * Authenticate use
     *
     * @return bool
     */
    protected function logUserIn()
    {
        $credentials = $this->request->only(['email', 'password']);
	
	    $credentials['status'] = 1;

        if (!Auth::attempt($credentials)) {
            return false;
        }

        return true;
    }
}
