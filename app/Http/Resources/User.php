<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
	    return [
	    	'id'=> $this->id,
		    'name' => $this->name,
		    'email' => $this->email,
		    'status' => $this->status,
		    'attributes' => $this->whenLoaded('userAttributes'),
		    'roles' => $this->whenLoaded('roles')
	    ];
    }
}
