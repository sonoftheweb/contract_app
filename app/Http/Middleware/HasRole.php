<?php

namespace App\Http\Middleware;

use App\Exceptions\NoAuthRoles;
use Auth;
use Closure;
use Illuminate\Http\Request;

class HasRole
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws NoAuthRoles
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user()->roles()->count())
            throw new NoAuthRoles;

        return $next($request);
    }
}
