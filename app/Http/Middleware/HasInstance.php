<?php

namespace App\Http\Middleware;

use App\Exceptions\UserHasNoInstance;
use App\Helpers\InstanceHelper;
use Closure;
use Illuminate\Http\Request;

class HasInstance
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws UserHasNoInstance
     */
    public function handle($request, Closure $next)
    {
        InstanceHelper::getInstance();

        return $next($request);
    }
}
