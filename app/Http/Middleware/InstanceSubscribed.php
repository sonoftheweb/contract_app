<?php

namespace App\Http\Middleware;

use App\Exceptions\InstanceNotSubscribedException;
use App\Exceptions\UserHasNoInstance;
use App\Helpers\InstanceHelper;
use Closure;
use Illuminate\Http\Request;

class InstanceSubscribed
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*$instance = InstanceHelper::getInstance();

        $subs = $instance->subscriptions()->first();

        if ($subs && $subs->stripe_status === 'active') {
            return $next($request);
        }

        throw new InstanceNotSubscribedException;*/
	
	    return $next($request);
    }
}
