<?php


namespace App\Helpers;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class SearchSortPaginateHelper
{
    public static function searchSortAndPaginate(Request $request, $query)
    {
        if ($request->has('search') && !empty($request->search))
            $query = SearchSortPaginateHelper::searchClauseBuilder($request, $query);

        if ($request->has('order') && !empty($request->order))
        	$query = SearchSortPaginateHelper::orderClauseBuilder($request, $query);

        if ($request->has('filter') && !empty($request->filter))
        	$query = SearchSortPaginateHelper::filterClauseBuilder($request, $query);

        $per_page = ($request->has('per_page')) ? $request->per_page : 10;
        return $query->paginate($per_page);
    }

    public static function searchClauseBuilder(Request $request, $query)
    {
        $table = $query->getModel()->getTable();

        switch ($table) {
            case 'users':
                $query->whereRaw("MATCH(name,email) AGAINST('" . $request->search . "')");
        }

        return $query;
    }

	private static function orderClauseBuilder(Request $request, $query)
	{
		$orderKey = explode('_', $request->order);

		if (count($orderKey) <= 1)
			return $query;

		$query = $query->orderByRaw($orderKey[1] . ' ' . $orderKey[0]);

		return $query;
	}

	private static function filterClauseBuilder(Request $request, $query)
	{
		$table = $query->getModel()->getTable();

		switch ($table) {
			case 'users':
				if (in_array($request->filter, ['manager', 'client'])) {
					$query = $query->whereHas('roles', function(Builder $builder) use ($request) {
						$builder->where('roles.id', config('constants.roles.account_' . $request->filter));
					});
				}
				break;
			default:
				// do nothing
		}

		return $query;
	}
}
