<?php


namespace App\Helpers;


use App\Exceptions\UserHasNoInstance;
use App\Models\Instance;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class InstanceHelper
{
    static $instance = null;

    /**
     * @return integer
     * @throws UserHasNoInstance
     */
    public static function getInstanceId()
    {
        $instance = self::getInstance();

        return $instance->id;
    }

    /**
     * @return Instance|BelongsTo|object
     * @throws UserHasNoInstance
     */
    public static function getInstance()
    {
        $user = Auth::user();

        $instance = $user->instance()->first();

        if (!$instance)
            throw new UserHasNoInstance;

        return static::$instance = $instance;
    }
	
	/**
	 * Builds subscription name from instance ID
	 *
	 * @param null $instance
	 * @param bool $premium
	 * @return string
	 * @throws UserHasNoInstance
	 */
	public static function subscriptionNameFromInstance($instance = null, $premium = false)
    {
        if (!$instance) {
            $instance = static::getInstance();
        }

        $name = 'subscription_'.$instance->id;
        
        if ($premium) {
        	$name .= '_premium';
        }
        
        return $name;
    }
}
