<?php

namespace App\Models;

use App\Exceptions\UserHasNoInstance;
use App\Helpers\InstanceHelper;
use Cache;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'instance_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $with = [
    	'userAttributes'
    ];

    public function instance()
    {
        return $this->belongsTo(Instance::class, 'instance_id', 'id');
    }

    public function userAttributes()
    {
        return $this->hasOne(UserAttribute::class, 'user_id', 'id');
    }

	/**
	 * @return BelongsToMany
	 * @throws UserHasNoInstance
	 */
	public function roles()
    {
    	return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id')
            ->wherePivot('instance_id', InstanceHelper::getInstanceId());
    }

    /**
     * If User has a given role, return true, else, return false
     *
     * @param string $role_name
     * @return bool
     * @throws UserHasNoInstance
     */
    public function hasRole(string $role_name): bool
    {
        $instanceId = InstanceHelper::getInstanceId();

        return Cache::remember('user_' . $this->getKey() . '_has_roles_' . $instanceId, env('CACHE_TIME_ONE_MINUTE'),
            function () use ($role_name) {
                if (is_string($role_name)) {
                    return $this->roles()->get()->contains('role_name', $role_name);
                }

                if (is_a($role_name, Role::class)) {
                    return !!$role_name->intersect($this->roles())->count();
                }

                return false;
            }
        );
    }

    /**
     * Assign a role by either role name or role object.
     *
     * @param $role
     * @throws UserHasNoInstance
     */
    public function assignRole($role)
    {
        if (is_string($role)) {
            $role = Role::query()->findOrFail(config('constants.roles.' . $role));
        }

        if (!is_a($role, Role::class))
            abort(404, 'Role passed is not a role object.');

        $this->roles()->attach($role, ['instance_id' => InstanceHelper::getInstanceId()]);
    }

    public function preference()
    {
        return $this->hasOne(UserPreference::class, 'user_id', 'id');
    }
}
