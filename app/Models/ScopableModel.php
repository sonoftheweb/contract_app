<?php

namespace App\Models;

use App\Helpers\InstanceHelper;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ScopableModel extends Model
{
	protected static function booted()
	{
		static::addGlobalScope('instance_id', function (Builder $builder) {
			$builder->where('instance_id', InstanceHelper::getInstanceId());
		});
	}
}
