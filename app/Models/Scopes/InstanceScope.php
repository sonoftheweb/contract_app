<?php


namespace App\Models\Scopes;


use App\Exceptions\UserHasNoInstance;
use App\Helpers\InstanceHelper;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class InstanceScope
{
	/**
	 * @param Builder $builder
	 * @param Model $model
	 * @throws UserHasNoInstance
	 */
	public function apply(Builder $builder, Model $model)
	{
		$builder->where('instance_id', InstanceHelper::getInstanceId());
	}
}