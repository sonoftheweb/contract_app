<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPreference extends Model
{
    protected $fillable = [
        'preferences',
        'user_id'
    ];

    protected $casts = [
        'preferences' => 'array',
    ];
}
