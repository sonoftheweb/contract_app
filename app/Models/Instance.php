<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;

class Instance extends Model
{
    use Billable;

    protected $table = 'instances';

    protected $fillable = [
        'instance_name',
        'account_manager_user_id',
        'logo',
        'seats',
        'address_line_1',
        'address_line_2',
        'city',
        'state',
        'country',
        'website',
        'direct_phone',
        'direct_email',
        'stripe_id',
        'card_brand',
        'card_last_four',
        'trial_ends_at'
    ];

    public function users()
    {
        return $this->hasMany(User::class, 'instance_id', 'id');
    }

    public function accountManagers()
    {
        return $this->users()->whereHas('roles', function (Builder $query) {
            $query->where('role_id', config('constants.roles.account_manager'));
        });
    }

    public function releaseSeat(int $seats = null)
    {
        $this->seats += ($seats) ? $seats : 1;
        $this->save();
    }

    public function occupySeat(int $seats = null)
    {
        $this->seats -= ($seats) ? $seats : 1;
        $this->save();
    }
}
