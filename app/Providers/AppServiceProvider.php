<?php

namespace App\Providers;

use Auth;
use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;
use Laravel\Cashier\Cashier;
use Session;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Cashier::ignoreMigrations();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Inertia::share('flash', function () {
            return Session::get('message');
        });
	
	    Inertia::share([
	    	'user' => function () {
	    	    return (!Auth::guest()) ? Auth::user()->with('instance')->first()->toArray() : null;
		    },
		    'authenticated' => function () {
	    	    return Auth::check();
		    }
	    ]);

        Inertia::share('app.name', config('app.name'));
        Inertia::share('validation_errors', function () {
            return session()->get('errors') ? session()->get('errors')->getBag('default')->getMessages() : (object) [];
        });
	
	    Inertia::version(function () {
		    return md5_file(public_path('mix-manifest.json'));
	    });
    }
    
    
}
