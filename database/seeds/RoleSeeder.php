<?php

use App\Models\Role;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws FileNotFoundException
     */
    public function run()
    {
        $json = File::get(base_path("database/data/roles.json"));
        $roles = json_decode($json, true);

        foreach ($roles as $role) {
            Role::query()->create($role);
        }
    }
}
