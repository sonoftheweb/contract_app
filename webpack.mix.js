const mix = require('laravel-mix');
require('vuetifyjs-mix-extension');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copyDirectory('resources/assets/images', 'public/images');
mix.js('resources/js/app.js', 'public/js');

if (mix.inProduction()) {
	mix.version();
}

mix.sass('resources/sass/app.scss', 'public/css')
	.vuetify('vuetify-loader');
