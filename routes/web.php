<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/', 'Auth\AuthController')
    ->except(['show', 'update', 'destroy'])
    ->names(['index' => 'login.index'])
    ->middleware('is_logged_in');

Route::resource('/registration', 'Auth\RegistrationController')
	->except(['show', 'update', 'destroy', 'index']);


Route::middleware(['auth', 'user_checks'])->group(function () {

    Route::middleware(['instance_checks'])->group(function () {
        Route::resource('dashboard', 'Admin\DashboardController');
    });

    Route::get('/logout', function(){
        Session::flush();
        Auth::logout();
        return Redirect::route('login.index')->with('message', array('type' => 'success', 'text' => 'You have successfully logged out'));
    });

    Route::prefix('account')->group(function () {
    	Route::resource('/', 'Admin\Account\AccountController')->only(['index', 'update', 'destroy']);
        Route::get('payment/{id}', 'Admin\Account\PaymentController@confirm')->name('confirm_payment');
        Route::post('payment', 'Admin\Account\PaymentController@beginPayment');
        Route::post('make-payment', 'Admin\Account\PaymentController@makePayment');
        Route::resource('payment-method', 'Admin\Account\PaymentMethodController')->only(['index', 'store']);

        Route::resource('subscription', 'Admin\Account\SubscriptionController')->only(['index', 'store']);
        Route::resource('users', 'Admin\Account\UserController');
    });

        /*Route::resource('application', 'Api\ApplicationController')->only(['index']);

        Route::get('plans', function () {
            return config('constants.plans');
        });

        Route::post('user-preferences', 'Api\UserPreferenceController@updatePreference');

        Route::resource('users', 'Api\UserController');

        Route::prefix('account')->group(function () {

            Route::resource('payment-method', 'Api\PaymentMethodController')->only(['index', 'store']);

            Route::resource('subscriptions', 'Api\SubscriptionController')->only(['index', 'store']);

        });*/

    });
