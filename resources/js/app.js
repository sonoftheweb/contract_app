import { InertiaApp } from '@inertiajs/inertia-vue'
import Vue from 'vue'
import vuetify from './plugins/vuetify'
import * as helpers from './Helpers'
import {loadStripe} from '@stripe/stripe-js/pure'

Vue.prototype.$helpers = helpers
Vue.prototype.$eventBus = new Vue();
Vue.prototype.$stripeLoader = {
    initialize: async () => {
        await loadStripe(process.env.MIX_STRIPE_KEY);
    }
}

Vue.use(InertiaApp)

const app = document.getElementById('app')

new Vue({
    vuetify,
    render: h => h(InertiaApp, {
        props: {
            initialPage: JSON.parse(app.dataset.page),
            resolveComponent: name => import(`./Pages/${name}`).then(module => module.default),
        },
    }),
}).$mount(app)
