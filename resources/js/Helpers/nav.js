import { Inertia } from '@inertiajs/inertia'

export const navigate = {
	goTo: (url, replace) => {
		let r = false
		
		if (replace) r = true
		
		Inertia.visit(url, {
			method: 'get',
			data: {},
			replace: r
		}).then(r => {
			console.log('(∩｀-´)⊃━━☆ﾟ.*･｡ﾟ Magic!')
		})
	}
}