import {rules} from './rules'
import {navigate} from "./nav";

export const helper = {
    rules: rules,
    nav: navigate,
    unitToWholePrice: (unit) => {
        return unit / 100
    },
    calculatePrice: (plan, seats) => {
        if (plan.tiers.length < 2)
            return  plan.tiers[0].price

        let tier_one = (plan.tiers) ? plan.tiers[0] : null,
            tier_two = (plan.tiers) ? plan.tiers[1] : null,
            calcPrice = 0

        if (tier_one && tier_two) {
            if (seats <= tier_one.upto) {
                calcPrice = tier_one.price
            } else {
                calcPrice = tier_one.price
                calcPrice += (seats - tier_one.upto) * tier_two.price
            }
        }

        return calcPrice
    },
    formatPrice: (cents) => {
        const formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2
        })
        return formatter.format(cents / 100)
    },
    formattedPrice: (price) => {
        let ps = price.split('.')

        return '<span>' + ps[0] + '.</span><small>' + ps[1] + '</small>'
    },
    firstLetterUpperCase: (string) => {
        return string.charAt(0).toUpperCase() + string.slice(1)
    }
}
