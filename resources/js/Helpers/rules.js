export const rules = {
    numericFieldRule: [
        (v) => !v || v === '' || new RegExp(/^-?[\d]+(\.[\d]+)?$/g).test(v) || "Please enter a numeric value",
    ],
    requiredFieldRule: [
        (v) => !!v || "Please fill in this field.",
    ],
    emailValidationRules: [
        (v) => !!v || "Please fill in your email address",
        (v) => /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(v) || "Please enter a valid email"
    ],
    avatarRules: [
        (v) => !v || v.size < 2000000 || 'Avatar size should be less than 2 MB!',
    ]
}
